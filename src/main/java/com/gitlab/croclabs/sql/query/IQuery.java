package com.gitlab.croclabs.sql.query;

import com.gitlab.croclabs.sql.query.AbstractQuery.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public interface IQuery<O> extends Query<O> {
	String getQuery();
	Result<O> getResult();
	IResult<O> getResultFunction();
	IValidate<O> getValidateFunction();
	IPrepare getPrepare();
	boolean isUpdate();

	default Result<O> execute() throws SQLException {
		return execute(QueryService.connectionSupplier.get());
	}

	default Result<O> execute(Connection connection) throws SQLException {
		Objects.requireNonNull(connection, "Connection should never be null!");

		try (PreparedStatement preparedStatement = connection.prepareStatement(getQuery())) {
			getPrepare().prepare(preparedStatement);

			if (isUpdate()) {
				return update(preparedStatement);
			} else {
				try (ResultSet resultSet = preparedStatement.executeQuery()) {
					return result(resultSet);
				}
			}
		}
	}

	private Result<O> result(ResultSet resultSet) throws SQLException {
		Result<O> res = getResult();

		while (resultSet.next()) {
			O obj = getResultFunction().result(resultSet);

			if (getValidateFunction().validate(res, obj)) {
				res.results.add(obj);
			}

			res.rows++;
		}

		return res;
	}

	@SuppressWarnings("unchecked")
	private Result<O> update(PreparedStatement preparedStatement) throws SQLException {
		Result<Boolean> resultBool = new Result<>();
		int rows = preparedStatement.executeUpdate();
		resultBool.rows = rows;
		resultBool.results.add(rows > 0);
		return (Result<O>) resultBool;
	}
}
