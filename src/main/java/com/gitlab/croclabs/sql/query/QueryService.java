package com.gitlab.croclabs.sql.query;

import java.sql.Connection;
import java.util.function.Supplier;

public class QueryService {
	protected static Supplier<Connection> connectionSupplier;

	public static void config(Supplier<Connection> connectionSupplier) {
		QueryService.connectionSupplier = connectionSupplier;
	}
}
