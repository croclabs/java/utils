package com.gitlab.croclabs.sql.query;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

public abstract class AbstractQuery<O> implements IQuery<O> {
	protected AbstractQuery() {
		super();
	}

	protected AbstractQuery(IQuery<O> query) {
		super();
		this.query = query.getQuery();
		this.validate = query.getValidateFunction();
		this.result = query.getResultFunction();
		this.update = query.isUpdate();
	}

	protected String query;
	protected IResult<O> result;
	protected IValidate<O> validate = ((results, lastResult) -> true);
	protected boolean update;
	protected Result<O> resultObj = new Result<>();
	protected IPrepare prepare;

	@Override
	public String getQuery() {
		return query;
	}

	@Override
	public Result<O> getResult() {
		return resultObj;
	}

	@Override
	public IResult<O> getResultFunction() {
		return result;
	}

	@Override
	public IValidate<O> getValidateFunction() {
		return validate;
	}

	@Override
	public IPrepare getPrepare() {
		return prepare;
	}

	@Override
	public boolean isUpdate() {
		return update;
	}

	protected abstract void prepare();

	public static class QueryBuilder<O> {
		AbstractQuery<O> query = new QueryBasic<>();

		QueryBuilder() {
			super();
		}

		public QueryBuilder<O> query(Object object) {
			query.query = object.toString();
			return this;
		}

		public QueryBuilder<O> query(Consumer<StringJoiner> joiner) {
			StringJoiner sj = new StringJoiner(" ");
			joiner.accept(sj);
			query.query = sj.toString();
			return this;
		}

		public QueryBuilder<O> query(Supplier<Object> supplier) {
			query.query = supplier.get().toString();
			return this;
		}

		public QueryBuilder<O> prepare(Object... objects) {
			query = new QueryVarargs<>(query);
			((QueryVarargs<O>) query).objects = objects;
			query.prepare();
			return this;
		}

		public QueryBuilder<O> prepare(IPrepareManual prepare) {
			query = new QueryManual<>(query);
			((QueryManual<O>) query).prepareManual = prepare;
			query.prepare();
			return this;
		}

		@SuppressWarnings("unchecked")
		public <T> QueryBuilder<T> result(IResult<T> result) {
			query.result = (IResult<O>) result;
			return (QueryBuilder<T>) this;
		}

		public QueryBuilder<O> validate(IValidate<O> validate) {
			query.validate = validate;
			return this;
		}

		@SuppressWarnings("unchecked")
		public QueryBuilder<Boolean> update() {
			query.update = true;
			return (QueryBuilder<Boolean>) this;
		}

		public Query<O> build() {
			return query;
		}
	}

	private static class QueryBasic<O> extends AbstractQuery<O> {
		public QueryBasic() {
			super();
		}

		public QueryBasic(IQuery<O> query) {
			super(query);
		}

		@Override
		public void prepare() {}
	}

	private static class QueryVarargs<O> extends AbstractQuery<O> {
		Object[] objects;

		public QueryVarargs(IQuery<O> query) {
			super(query);
		}

		@Override
		public void prepare() {
			this.prepare = (preparedStatement) -> {
				int index = 0;

				for (Object object : objects) {
					preparedStatement.setObject(++index, Objects.requireNonNullElse(object, ""));
				}
			};
		}
	}

	private static class QueryManual<O> extends AbstractQuery<O> {
		IPrepareManual prepareManual;

		public QueryManual(IQuery<O> query) {
			super(query);
		}

		@Override
		public void prepare() {
			this.prepare = (preparedStatement) -> prepareManual.prepare(preparedStatement, 0);
		}
	}

	public static class Result<O> {
		List<O> results = new ArrayList<>();
		int rows = 0;

		public int rows() {
			return rows;
		}

		public Collection<O> get() {
			return new ArrayList<>(results);
		}

		public Optional<O> first() {
			return get(0);
		}

		public Optional<O> get(int i) {
			if (results.isEmpty()) {
				return Optional.empty();
			}

			return Optional.of(results.get(i));
		}

		public Stream<O> stream() {
			return results.stream();
		}
	}

	@FunctionalInterface
	public interface IResult<O> {
		O result(ResultSet resultSet) throws SQLException;
	}

	@FunctionalInterface
	public interface IValidate<O> {
		boolean validate(Result<O> results, O lastResult) throws SQLException;
	}

	@FunctionalInterface
	public interface IPrepare {
		void prepare(PreparedStatement preparedStatement) throws SQLException;
	}

	@FunctionalInterface
	public interface IPrepareManual {
		void prepare(PreparedStatement preparedStatement, int index) throws SQLException;
	}
}
