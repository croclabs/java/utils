package com.gitlab.croclabs.sql.query;

import com.gitlab.croclabs.sql.query.AbstractQuery.QueryBuilder;
import com.gitlab.croclabs.sql.query.AbstractQuery.Result;

import java.sql.Connection;
import java.sql.SQLException;

public interface Query<O> {
	Result<O> execute() throws SQLException;
	Result<O> execute(Connection connection) throws SQLException;

	static QueryBuilder<Object> builder() {
		return new QueryBuilder<>();
	}
}
