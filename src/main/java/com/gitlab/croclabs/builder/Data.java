package com.gitlab.croclabs.builder;

import com.gitlab.croclabs.builder.Data.Builder;

public abstract class Data<B extends Builder<?>> implements IData<B> {
	protected static abstract class Builder<BD extends Data<?>> implements IBuilder<BD> {
		private BD instance;

		Builder(BD instance) {
			super();
			this.instance = instance;
		}

		@Override
		public BD build() {
			instance.validate();
			return instance;
		}
	}
}
