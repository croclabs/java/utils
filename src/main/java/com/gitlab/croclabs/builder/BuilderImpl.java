package com.gitlab.croclabs.builder;

import com.gitlab.croclabs.builder.BuilderImpl.BuilderB;

class BuilderImpl extends Data<BuilderB> {

	@Override
	public BuilderB builder() {
		return new BuilderImpl().new BuilderB();
	}

	@Override
	public void validate() {
	}

	public class BuilderB extends Builder<BuilderImpl> {

		BuilderB() {
			super(BuilderImpl.this);
		}
	}
}
