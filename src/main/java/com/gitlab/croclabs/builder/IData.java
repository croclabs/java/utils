package com.gitlab.croclabs.builder;

import com.gitlab.croclabs.builder.IData.IBuilder;

public interface IData<B extends IBuilder<?>> {
	B builder();
	void validate();

	interface IBuilder<BD extends IData<?>> {
		BD build();
	}
}
