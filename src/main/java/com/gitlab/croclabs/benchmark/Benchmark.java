package com.gitlab.croclabs.benchmark;

import com.gitlab.croclabs.time.Duration;

import java.time.Instant;
import java.util.UUID;
import java.util.function.Supplier;
import java.util.logging.Logger;

public class Benchmark {
	private static final Logger LOG = Logger.getLogger(Benchmark.class.getName());

	private Benchmark() {
		super();
	}

	public static <T> T benchmark(Supplier<T> action) {
		return benchmark(action, UUID.randomUUID());
	}

	public static void benchmark(Procedure action) {
		benchmark(() -> {
			action.action();
			return null;
		});
	}

	public static void benchmark(Procedure action, Object id) {
		benchmark(() -> {
			action.action();
			return null;
		}, id);
	}

	public static <T> T benchmark(Supplier<T> action, Object id) {
		long start = Instant.now().toEpochMilli();
		T result = action.get();
		long end = Instant.now().toEpochMilli();
		LOG.info("Benchmark \"" + id.toString() + "\" took: " + Duration.millisToMinSecsMillis(end - start));
		return result;
	}

	@FunctionalInterface    public interface Procedure {
		void action();
	}
}
