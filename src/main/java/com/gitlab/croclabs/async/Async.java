package com.gitlab.croclabs.async;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;
import java.util.logging.Logger;

public interface Async<T> {
	CompletableFuture<T> async();

	static <T> CompletableFuture<T> async(Supplier<T> supplier) {
		return CompletableFuture.supplyAsync(supplier);
	}

	static CompletableFuture<Collection<Object>> async(Supplier<?>... supplier) {
		return async(null, supplier);
	}

	static CompletableFuture<Collection<Object>> async(CompletableFuture<?>... futures) {
		return async(null, futures);
	}

	static CompletableFuture<Collection<Object>> async(Async<?>... asyncs) {
		return async(null, asyncs);
	}

	static CompletableFuture<Collection<Object>> async(Timeout timeout, Supplier<?>... supplier) {
		Collection<CompletableFuture<?>> futures = new ArrayList<>();
		Arrays.asList(supplier).forEach(sup -> futures.add(CompletableFuture.supplyAsync(sup)));
		return async(timeout, futures.toArray(CompletableFuture[]::new));
	}

	static CompletableFuture<Collection<Object>> async(Timeout timeout, Async<?>... asyncs) {
		Collection<CompletableFuture<?>> futures = new ArrayList<>();
		Arrays.asList(asyncs).forEach(async -> futures.add(async.async()));
		return async(timeout, futures.toArray(CompletableFuture[]::new));
	}

	static CompletableFuture<Collection<Object>> async(Timeout timeout, CompletableFuture<?>... futures) {
		return CompletableFuture.supplyAsync(() -> {
			Collection<Object> results = new ArrayList<>();
			Arrays.asList(futures).forEach(future -> addResult(timeout, future, results));
			return results;
		});
	}

	private static void addResult(Timeout timeout, CompletableFuture<?> future, Collection<Object> results) {
		try {
			if (timeout == null) {
				results.add(future.get());
			} else {
				results.add(future.get(timeout.time(), timeout.unit()));
			}
		} catch (InterruptedException e) {
			log().severe("Async thread interrupted");
			e.printStackTrace();
			Thread.currentThread().interrupt();
		} catch (ExecutionException | TimeoutException e) {
			log().severe("Getting async result failed");
			e.printStackTrace();
			results.add(null);
		}
	}

	private static Logger log() {
		return Logger.getLogger(Async.class.getName());
	}

	class Timeout {
		long time;
		TimeUnit unit;

		private Timeout(long time, TimeUnit unit) {
			this.time = time;
			this.unit = unit;
		}

		public static Timeout sec(long time) {
			return new Timeout(time, TimeUnit.SECONDS);
		}

		public static Timeout min(long time) {
			return new Timeout(time, TimeUnit.MINUTES);
		}

		public static Timeout hour(long time) {
			return new Timeout(time, TimeUnit.HOURS);
		}

		public long time() {
			return time;
		}

		public TimeUnit unit() {
			return unit;
		}
	}
}
