package com.gitlab.croclabs.generics;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public interface Generic {
	default List<Class<?>> types() {
		List<Class<?>> classes = new ArrayList<>();

		for (Type type : ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()) {
			if (type instanceof Class<?>) {
				classes.add((Class<?>) type);
			} else {
				throw new RuntimeException("Error getting type");
			}
		}

		return classes;
	}

	default List<Object> instances() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		List<Object> obj = new ArrayList<>();

		for (Class<?> clazz : types()) {
			obj.add(construct(clazz));
		}

		return obj;
	}

	@SuppressWarnings("unchecked")
	default <K> K instance(int index) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		return (K) construct(types().get(index));
	}

	private Object construct(Class<?> clazz) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		if (clazz == Integer.class) {
			return clazz.getDeclaredConstructor(int.class).newInstance(0);
		}

		if (clazz == Long.class) {
			return clazz.getDeclaredConstructor(long.class).newInstance(0);
		}

		if (clazz == Boolean.class) {
			return clazz.getDeclaredConstructor(boolean.class).newInstance(false);
		}

		// TODO add rest of primitive types

		return clazz.getDeclaredConstructor().newInstance();
	}
}
