package com.gitlab.croclabs.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;

import java.io.*;
import java.util.Arrays;
import java.util.Objects;
import java.util.logging.Logger;

public class JSONComposer<T> implements JSONCompiler<T, Feature> {
	private static final Logger LOG = Logger.getLogger(JSONComposer.class.getName());
	protected JsonFactory factory = new JsonFactory();
	protected T object;
	protected boolean pretty;
	protected JsonMapper mapper;

	protected JSONComposer(T object) {
		factory.enable(Feature.IGNORE_UNKNOWN)
				.enable(Feature.WRITE_BIGDECIMAL_AS_PLAIN);

		this.object = object;
	}

	public JSONComposer<T> enable(Feature... feature) {
		Arrays.stream(feature).forEach(f -> factory.enable(f));
		return this;
	}

	public JSONComposer<T> disable(Feature... feature) {
		Arrays.stream(feature).forEach(f -> factory.disable(f));
		return this;
	}

	@SuppressWarnings("unchecked")
	public <P> JSONComposer<P> object(P object) {
		this.object = (T) object;
		return (JSONComposer<P>) this;
	}

	public JSONComposer<T> pretty() {
		pretty = true;
		return this;
	}

	public String compose() {
		createMapper();

		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			LOG.severe("Writing json failed");
			e.printStackTrace();
			return "";
		}
	}

	public byte[] composeBytes() {
		createMapper();

		try {
			return mapper.writeValueAsBytes(object);
		} catch (JsonProcessingException e) {
			LOG.severe("Writing json failed");
			e.printStackTrace();
			return new byte[0];
		}
	}

	public void compose(Object out) {
		createMapper();

		try {
			if (out instanceof Writer) {
				mapper.writeValue((Writer) out, object);
			} else if (out instanceof DataOutput) {
				mapper.writeValue((DataOutput) out, object);
			} else if (out instanceof File) {
				mapper.writeValue((File) out, object);
			} else if (out instanceof OutputStream) {
				mapper.writeValue((OutputStream) out, object);
			} else {
				LOG.severe("Unsupported output type");
			}
		} catch (IOException e) {
			LOG.severe("Writing json failed");
			e.printStackTrace();
		}
	}

	protected void createMapper() {
		Objects.requireNonNull(
				object,
				"No object found to compose JSON from, make sure to set it via .object() or in JSON.composer()!"
		);

		mapper = new JsonMapper(factory);

		if (pretty) {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
		}
	}
}
