package com.gitlab.croclabs.builder;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BuilderImplTest {

	@Test
	void validate() {
		BuilderImpl impl = new BuilderImpl()
				.builder()
				.build();
		Assertions.assertNotNull(impl);
	}
}